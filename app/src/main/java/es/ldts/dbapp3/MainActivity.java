package es.ldts.dbapp3;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

	private static Context context;

	private ActionBarDrawerToggle mDrawerToggle;

	private TextView pizarra;
	private Button refresh;
	//linea borrada aquí...

	private android.support.v7.widget.ShareActionProvider mShareActionProvider = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		context = getBaseContext();
		setContentView(R.layout.activity_main);
		filemanager.setbook(getCacheDir().getAbsolutePath(), context);

		pizarra = (TextView) findViewById(R.id.pizarra);
		refresh = (Button) findViewById(R.id.refresh);
		//boton2 = (Button) findViewById(R.id.boton2);

		// enabling action bar app icon and behaving it as toggle button

		mDrawerToggle = new ActionBarDrawerToggle(this, null,
				R.mipmap.ic_navigation_drawer, //nav menu toggle icon
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
		);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

        setTitle(context.getResources().getString(R.string.app_name));
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		getSupportActionBar().hide();

		wrpizzara();

	}

	public void wrpizzara() {
        if (!db.isOpen()) db.open();
		String[][] aux = db.select("Select * from usuarios order by random()");

		String texto = "Listado De Usuarios\n";
		if (aux != null)
			for (int i = 0; i < aux.length; i++) {
				if (aux[i] != null)
					for (int j = 0; j < aux[i].length; j++)
						texto += aux[i][j] + " ";
				texto += "\n";
			}

        texto += "\n" +
                "Y por cierto, ¿DONDE ESTÁ LA BARRA DE ARRIBA CON EL NOMBRE DE AL APPLICACIÓN?";

		pizarra.setText(texto);
	}

	public void wrpizzara(View v) {
		wrpizzara();
	}

		@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.share_menu, menu);
		MenuItem itemShare = menu.findItem(R.id.menu_item_share);
		itemShare.setIcon(R.mipmap.share);
		// Fetch and store ShareActionProvider
		mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(itemShare);
		Intent j = new Intent(Intent.ACTION_SEND);
		setShareIntent(j);
		doShare();
		// Return true to display menu
		return true;
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		return true;
	}


	// SHARE BUTTON!!!!
	// Call to update the share intent
	//@Override
	private void setShareIntent(Intent shareIntent) {
		if (mShareActionProvider != null) {
			mShareActionProvider.setShareIntent(shareIntent);
		}
	}

	public void doShare() {
		// populate the share intent with data
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TEXT, context.getResources().getString(R.string.sharetxt) +
				"\n" + context.getResources().getString(R.string.app_name) + "\n" +
				context.getResources().getString(R.string.sharelink)
		);
		mShareActionProvider.setShareIntent(intent);
	}

	private void exit() {
		new AlertDialog.Builder(this)
				.setTitle(R.string.app_name)
				.setMessage(context.getResources().getStringArray(R.array.exitbuttom)[0])
				.setPositiveButton(context.getResources().getStringArray(R.array.exitbuttom)[1], new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						//finish();
						//finishActivity(0);
						System.exit(0);
					}
				})
				.setNegativeButton(context.getResources().getStringArray(R.array.exitbuttom)[2], new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
					}
				})
				.show();
	}


	@Override
	public void setTitle(CharSequence title) {
		getSupportActionBar().setTitle(title);
	}



	@Override
	public void onBackPressed() {
		//your code when back button pressed
        exit();
	}


}
